package com.example.transaction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.transaction.response.CompanyResponse;
import com.example.transaction.service.HelperService;


@RestController
@RequestMapping("/api")
public class CompanyController {
	
	@Autowired
	private HelperService helperService ;
	
	@PostMapping("/createCompany")
	public CompanyResponse createCompany() {
		return helperService.createCompany();
	}

	@PostMapping("/createCompanies")
	public CompanyResponse createCompanies() {
		return helperService.createCompanies();
	}
}
