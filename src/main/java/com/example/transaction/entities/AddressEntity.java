package com.example.transaction.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "ADDRESS")
public class AddressEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ADDRESS_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int addressId;
	
	@Column(name = "ADDRESS_LINE1")
	private String addressLine1;
	
	@Column(name = "ADDRESS_LINE2")
	private String addressLine2;
	
	@Column(name = "CITY")
	private String city;
	
	@Column(name = "STATE")
	private String state;
	
	@Column(name = "ZIPCODE")
	private String zipCode;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "ERROR")
	private String error;
	
	@OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.MERGE, mappedBy = "addressEntity")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyEntity companyEntity;
	
	@OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.MERGE, mappedBy = "addressEntity")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private EmployeeEntity employeeEntity;
	
}
