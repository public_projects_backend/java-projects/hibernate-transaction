package com.example.transaction.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.example.transaction.entities.EmployeeEntity;
import com.example.transaction.model.EmployeeDetails;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
	
	@Mapping(source = "addressEntity", target = "addressDetails")
	EmployeeDetails toPojo(EmployeeEntity entity);
	
	@Mapping(source = "addressDetails", target = "addressEntity")
	EmployeeEntity toEntity(EmployeeDetails details);
}


