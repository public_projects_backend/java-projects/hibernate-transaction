package com.example.transaction.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.example.transaction.entities.CompanyEntity;
import com.example.transaction.model.CompanyDetails;

@Mapper(componentModel = "spring")
public interface CompanyMapper {

	@Mapping(source = "addressEntity", target = "addressDetails")
	CompanyDetails toPojo(CompanyEntity entity);

	@Mapping(source = "addressDetails", target = "addressEntity")
	CompanyEntity toEntity(CompanyDetails details);
}
