package com.example.transaction.mapper;

import org.mapstruct.Mapper;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.model.AddressDetails;

@Mapper(componentModel = "spring")
public interface AddressMapper {
	AddressDetails toPojo(AddressEntity entity);

	AddressEntity toEntity(AddressDetails details);
}
