package com.example.transaction.response;


import java.util.List;

import com.example.transaction.model.CompanyDetails;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class CompanyResponse {
	private CompanyDetails companyDetails;
	
	private List<CompanyDetails> companyDetailsList;
}
