package com.example.transaction.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.transaction.entities.AddressEntity;

@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, Integer>  {
	
	@Modifying
	@Query("UPDATE AddressEntity SET STATUS = ?2, ERROR = ?3 WHERE ADDRESS_ID = ?1")
	void updateAddressEntity(int addressId, String status, String error);
}
