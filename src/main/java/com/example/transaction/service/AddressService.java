package com.example.transaction.service;

import java.util.List;

import com.example.transaction.entities.AddressEntity;

public interface AddressService {
	AddressEntity saveAddress();
	
	List<AddressEntity> saveAddresses();
	
	void updateAddress(AddressEntity addressEntity);
}
