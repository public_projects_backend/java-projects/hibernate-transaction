package com.example.transaction.service;

import java.util.List;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.entities.EmployeeEntity;
import com.example.transaction.model.EmployeeDetails;

public interface EmployeeService {
	void createEmployee(AddressEntity addressEntity);
	
	List<EmployeeEntity> prepareEmployeeList(AddressEntity addressEntity);

	List<EmployeeDetails> createEmployees(List<EmployeeEntity> employeeEntities);
}
