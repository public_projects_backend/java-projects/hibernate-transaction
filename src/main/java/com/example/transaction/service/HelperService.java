package com.example.transaction.service;

import com.example.transaction.response.CompanyResponse;

public interface HelperService {
	CompanyResponse createCompany();
	
	CompanyResponse createCompanies();
}
