package com.example.transaction.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.entities.CompanyEntity;
import com.example.transaction.entities.EmployeeEntity;
import com.example.transaction.model.CompanyDetails;
import com.example.transaction.service.AddressService;
import com.example.transaction.service.CompanyService;
import com.example.transaction.service.EmployeeService;

@Service
public class ExtractedClassImpl {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private AddressService addressService;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void extracted(List<CompanyDetails> companiesList, List<CompanyEntity> companyEntities,
			List<EmployeeEntity> employeeEntities, AddressEntity address) {
		
			companiesList.addAll(companyService.createCompanies(companyEntities));
			employeeService.createEmployees(employeeEntities);
			address.setStatus("Completed");
			addressService.updateAddress(address);
		
	}
}
