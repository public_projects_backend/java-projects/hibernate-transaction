package com.example.transaction.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.entities.EmployeeEntity;
import com.example.transaction.mapper.EmployeeMapper;
import com.example.transaction.model.EmployeeDetails;
import com.example.transaction.repo.EmployeeRepository;
import com.example.transaction.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeMapper employeeMapper;

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = NullPointerException.class)
	public void createEmployee(AddressEntity addressEntity) {
		EmployeeDetails employeeDetails = new EmployeeDetails();
		employeeDetails.setFirstName("Chethan");
		employeeDetails.setLastName("Kumar");
		EmployeeEntity employeeEntity = employeeMapper.toEntity(employeeDetails);
		employeeEntity.setAddressEntity(addressEntity);
		ArrayList alist = null;
		// System.out.println(alist.size());
		employeeRepository.save(employeeEntity);

	}

	@Override
	public List<EmployeeEntity> prepareEmployeeList(AddressEntity addressEntity) {

		EmployeeDetails employeeDetails1 = new EmployeeDetails();
		employeeDetails1.setFirstName("Test");
		employeeDetails1.setLastName("User 1");
		EmployeeEntity employeeEntity1 = employeeMapper.toEntity(employeeDetails1);
		employeeEntity1.setAddressEntity(addressEntity);

		EmployeeDetails employeeDetails2 = new EmployeeDetails();
		employeeDetails2.setFirstName("Test");
		employeeDetails2.setLastName("User 2");
		EmployeeEntity employeeEntity2 = employeeMapper.toEntity(employeeDetails2);
		employeeEntity2.setAddressEntity(addressEntity);

		EmployeeDetails employeeDetails3 = new EmployeeDetails();
		employeeDetails3.setFirstName("Test");
		employeeDetails3.setLastName("User 3");
		EmployeeEntity employeeEntity3 = employeeMapper.toEntity(employeeDetails3);
		employeeEntity3.setAddressEntity(addressEntity);

		EmployeeDetails employeeDetails4 = new EmployeeDetails();
		employeeDetails4.setFirstName("Test");
		employeeDetails4.setLastName("User 4");
		EmployeeEntity employeeEntity4 = employeeMapper.toEntity(employeeDetails4);
		employeeEntity4.setAddressEntity(addressEntity);

		EmployeeDetails employeeDetails5 = new EmployeeDetails();
		employeeDetails5.setFirstName("Test");
		employeeDetails5.setLastName("User 5");
		EmployeeEntity employeeEntity5 = employeeMapper.toEntity(employeeDetails5);
		employeeEntity5.setAddressEntity(addressEntity);

		return Arrays.asList(employeeEntity1, employeeEntity2, employeeEntity3, employeeEntity4, employeeEntity5);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<EmployeeDetails> createEmployees(List<EmployeeEntity> employeeEntities) {
		employeeRepository.saveAll(employeeEntities);
		List<EmployeeDetails> employeeDetailsList = employeeEntities.stream()
				.map(entity -> employeeMapper.toPojo(entity)).collect(Collectors.toList());

		employeeEntities.forEach(companyEntity -> {
			if (companyEntity.getAddressEntity().getAddressId() == 3) {
				var x = 10 / 0;
			}
		});
		return employeeDetailsList;
	}
}
