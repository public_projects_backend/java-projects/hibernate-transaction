package com.example.transaction.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.mapper.AddressMapper;
import com.example.transaction.model.AddressDetails;
import com.example.transaction.repo.AddressRepository;
import com.example.transaction.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService {
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private AddressMapper addressMapper;
	
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = NullPointerException.class)
	public AddressEntity saveAddress() { //Status(InProgress,Completed, Error), Error(Error Message)
		AddressDetails addressDetails = new AddressDetails("Test1", "Test2", "Bengaluru", "Karnataka", "560066", "InProgress", "");
		AddressEntity  addressEntity = addressMapper.toEntity(addressDetails);
		addressRepository.save(addressEntity);
		return addressEntity;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AddressEntity> saveAddresses() {
		AddressEntity  addressEntity1 = addressMapper.toEntity(new AddressDetails("Test1", "Test2", "Bengaluru", "Karnataka", "560066", "InProgress", ""));
		AddressEntity  addressEntity2 = addressMapper.toEntity(new AddressDetails("Test1", "Test2", "Chennai", "Tamil Nadu", "560066", "InProgress", ""));
		AddressEntity  addressEntity3 = addressMapper.toEntity(new AddressDetails("Test1", "Test2", "Mumbai", "Maharastra", "560066", "InProgress", ""));
		List<AddressEntity> addressEntities = Arrays.asList(addressEntity1, addressEntity2, addressEntity3);
		addressRepository.saveAll(addressEntities);
		return addressEntities;
	}

	@Override
	@Transactional(propagation = Propagation.MANDATORY)
	public void updateAddress(AddressEntity addressEntity) {
		addressRepository.updateAddressEntity(addressEntity.getAddressId(), addressEntity.getStatus(), addressEntity.getError());
	}
}
