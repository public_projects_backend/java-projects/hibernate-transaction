package com.example.transaction.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.entities.CompanyEntity;
import com.example.transaction.entities.EmployeeEntity;
import com.example.transaction.model.CompanyDetails;
import com.example.transaction.response.CompanyResponse;
import com.example.transaction.service.AddressService;
import com.example.transaction.service.CompanyService;
import com.example.transaction.service.EmployeeService;
import com.example.transaction.service.HelperService;

@Service
public class HelperServiceImpl implements HelperService {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private ExtractedClassImpl extractedClassImpl;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CompanyResponse createCompany() {
		AddressEntity addressEntity = null;
		addressEntity = addressService.saveAddress();
		CompanyDetails companyDetails = companyService.createCompany(addressEntity);
		employeeService.createEmployee(addressEntity);
		return CompanyResponse.builder().companyDetails(companyDetails).build();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CompanyResponse createCompanies() {
		List<CompanyDetails> companiesList = new ArrayList<>();
		List<AddressEntity> addressEntities = addressService.saveAddresses();
		addressEntities.forEach(address -> {
			List<CompanyEntity> companyEntities = companyService.prepareCompanyList(address);
			List<EmployeeEntity> employeeEntities = employeeService.prepareEmployeeList(address);
			try {
				extractedClassImpl.extracted(companiesList, companyEntities, employeeEntities, address);
			} catch (Exception e) {
				address.setStatus("Error");
				address.setError(e.getMessage());
				addressService.updateAddress(address);
				e.printStackTrace();
			}
		});
		return CompanyResponse.builder().companyDetailsList(companiesList).build();
	}

}
