package com.example.transaction.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.entities.CompanyEntity;
import com.example.transaction.mapper.CompanyMapper;
import com.example.transaction.model.CompanyDetails;
import com.example.transaction.repo.CompanyRepository;
import com.example.transaction.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyMapper companyMapper;

	@Autowired
	private CompanyRepository companyRepository;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = NullPointerException.class)
	public CompanyDetails createCompany(AddressEntity addressEntity) {
		CompanyDetails companyDetails = new CompanyDetails();
		companyDetails.setCompanyName("KineLabs Private Limited");
		CompanyEntity companyEntity = companyMapper.toEntity(companyDetails);
		companyEntity.setAddressEntity(addressEntity);
		companyRepository.save(companyEntity);
		return companyDetails;
	}

	@Override
	public List<CompanyEntity> prepareCompanyList(AddressEntity addressEntity) {
		
		CompanyDetails companyDetails1 = new CompanyDetails();
		companyDetails1.setCompanyName("KineLabs Private Limited - 1");
		CompanyEntity companyEntity1 = companyMapper.toEntity(companyDetails1);
		companyEntity1.setAddressEntity(addressEntity);

		CompanyDetails companyDetails2 = new CompanyDetails();
		companyDetails2.setCompanyName("KineLabs Private Limited - 2");
		CompanyEntity companyEntity2 = companyMapper.toEntity(companyDetails2);
		companyEntity2.setAddressEntity(addressEntity);

		CompanyDetails companyDetails3 = new CompanyDetails();
		companyDetails3.setCompanyName("KineLabs Private Limited - 3");
		CompanyEntity companyEntity3 = companyMapper.toEntity(companyDetails3);
		companyEntity3.setAddressEntity(addressEntity);

		CompanyDetails companyDetails4 = new CompanyDetails();
		companyDetails4.setCompanyName("KineLabs Private Limited - 4");
		CompanyEntity companyEntity4 = companyMapper.toEntity(companyDetails4);
		companyEntity4.setAddressEntity(addressEntity);

		CompanyDetails companyDetails5 = new CompanyDetails();
		companyDetails5.setCompanyName("KineLabs Private Limited - 5");
		CompanyEntity companyEntity5 = companyMapper.toEntity(companyDetails5);
		companyEntity5.setAddressEntity(addressEntity);
		
		return Arrays.asList(companyEntity1, companyEntity2, companyEntity3, companyEntity4, companyEntity5);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<CompanyDetails> createCompanies(List<CompanyEntity> companyEntities) {
		companyRepository.saveAll(companyEntities);
		List<CompanyDetails> companyDetailsList = new ArrayList<>();
		companyEntities.forEach(companyEntity -> {
			CompanyDetails companyDetails = companyMapper.toPojo(companyEntity);
			companyDetailsList.add(companyDetails);
		});
		companyEntities.forEach(companyEntity -> {
			if (companyEntity.getAddressEntity().getAddressId() == 2) {
				var x = 10/0;
			}
		});
		return companyDetailsList;
	}
}
