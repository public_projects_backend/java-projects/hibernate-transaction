package com.example.transaction.service;

import java.util.List;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.entities.CompanyEntity;
import com.example.transaction.model.CompanyDetails;

public interface CompanyService {
	CompanyDetails createCompany(AddressEntity addressEntity);
	
	List<CompanyEntity> prepareCompanyList(AddressEntity addressEntity);
	
	List<CompanyDetails> createCompanies(List<CompanyEntity> companyEntities);
}
