package com.example.transaction.mapper;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.entities.EmployeeEntity;
import com.example.transaction.model.AddressDetails;
import com.example.transaction.model.EmployeeDetails;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-08T22:44:13+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class EmployeeMapperImpl implements EmployeeMapper {

    @Override
    public EmployeeDetails toPojo(EmployeeEntity entity) {
        if ( entity == null ) {
            return null;
        }

        EmployeeDetails employeeDetails = new EmployeeDetails();

        employeeDetails.setAddressDetails( addressEntityToAddressDetails( entity.getAddressEntity() ) );
        employeeDetails.setFirstName( entity.getFirstName() );
        employeeDetails.setLastName( entity.getLastName() );

        return employeeDetails;
    }

    @Override
    public EmployeeEntity toEntity(EmployeeDetails details) {
        if ( details == null ) {
            return null;
        }

        EmployeeEntity employeeEntity = new EmployeeEntity();

        employeeEntity.setAddressEntity( addressDetailsToAddressEntity( details.getAddressDetails() ) );
        employeeEntity.setFirstName( details.getFirstName() );
        employeeEntity.setLastName( details.getLastName() );

        return employeeEntity;
    }

    protected AddressDetails addressEntityToAddressDetails(AddressEntity addressEntity) {
        if ( addressEntity == null ) {
            return null;
        }

        AddressDetails addressDetails = new AddressDetails();

        addressDetails.setAddressLine1( addressEntity.getAddressLine1() );
        addressDetails.setAddressLine2( addressEntity.getAddressLine2() );
        addressDetails.setCity( addressEntity.getCity() );
        addressDetails.setState( addressEntity.getState() );
        addressDetails.setZipCode( addressEntity.getZipCode() );
        addressDetails.setStatus( addressEntity.getStatus() );
        addressDetails.setError( addressEntity.getError() );

        return addressDetails;
    }

    protected AddressEntity addressDetailsToAddressEntity(AddressDetails addressDetails) {
        if ( addressDetails == null ) {
            return null;
        }

        AddressEntity addressEntity = new AddressEntity();

        addressEntity.setAddressLine1( addressDetails.getAddressLine1() );
        addressEntity.setAddressLine2( addressDetails.getAddressLine2() );
        addressEntity.setCity( addressDetails.getCity() );
        addressEntity.setState( addressDetails.getState() );
        addressEntity.setZipCode( addressDetails.getZipCode() );
        addressEntity.setStatus( addressDetails.getStatus() );
        addressEntity.setError( addressDetails.getError() );

        return addressEntity;
    }
}
