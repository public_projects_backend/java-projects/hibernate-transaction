package com.example.transaction.mapper;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.model.AddressDetails;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-08T22:44:13+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class AddressMapperImpl implements AddressMapper {

    @Override
    public AddressDetails toPojo(AddressEntity entity) {
        if ( entity == null ) {
            return null;
        }

        AddressDetails addressDetails = new AddressDetails();

        addressDetails.setAddressLine1( entity.getAddressLine1() );
        addressDetails.setAddressLine2( entity.getAddressLine2() );
        addressDetails.setCity( entity.getCity() );
        addressDetails.setState( entity.getState() );
        addressDetails.setZipCode( entity.getZipCode() );
        addressDetails.setStatus( entity.getStatus() );
        addressDetails.setError( entity.getError() );

        return addressDetails;
    }

    @Override
    public AddressEntity toEntity(AddressDetails details) {
        if ( details == null ) {
            return null;
        }

        AddressEntity addressEntity = new AddressEntity();

        addressEntity.setAddressLine1( details.getAddressLine1() );
        addressEntity.setAddressLine2( details.getAddressLine2() );
        addressEntity.setCity( details.getCity() );
        addressEntity.setState( details.getState() );
        addressEntity.setZipCode( details.getZipCode() );
        addressEntity.setStatus( details.getStatus() );
        addressEntity.setError( details.getError() );

        return addressEntity;
    }
}
