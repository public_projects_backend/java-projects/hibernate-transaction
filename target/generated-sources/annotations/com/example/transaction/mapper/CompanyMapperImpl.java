package com.example.transaction.mapper;

import com.example.transaction.entities.AddressEntity;
import com.example.transaction.entities.CompanyEntity;
import com.example.transaction.model.AddressDetails;
import com.example.transaction.model.CompanyDetails;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-08T22:44:12+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class CompanyMapperImpl implements CompanyMapper {

    @Override
    public CompanyDetails toPojo(CompanyEntity entity) {
        if ( entity == null ) {
            return null;
        }

        CompanyDetails companyDetails = new CompanyDetails();

        companyDetails.setAddressDetails( addressEntityToAddressDetails( entity.getAddressEntity() ) );
        companyDetails.setCompanyName( entity.getCompanyName() );

        return companyDetails;
    }

    @Override
    public CompanyEntity toEntity(CompanyDetails details) {
        if ( details == null ) {
            return null;
        }

        CompanyEntity companyEntity = new CompanyEntity();

        companyEntity.setAddressEntity( addressDetailsToAddressEntity( details.getAddressDetails() ) );
        companyEntity.setCompanyName( details.getCompanyName() );

        return companyEntity;
    }

    protected AddressDetails addressEntityToAddressDetails(AddressEntity addressEntity) {
        if ( addressEntity == null ) {
            return null;
        }

        AddressDetails addressDetails = new AddressDetails();

        addressDetails.setAddressLine1( addressEntity.getAddressLine1() );
        addressDetails.setAddressLine2( addressEntity.getAddressLine2() );
        addressDetails.setCity( addressEntity.getCity() );
        addressDetails.setState( addressEntity.getState() );
        addressDetails.setZipCode( addressEntity.getZipCode() );
        addressDetails.setStatus( addressEntity.getStatus() );
        addressDetails.setError( addressEntity.getError() );

        return addressDetails;
    }

    protected AddressEntity addressDetailsToAddressEntity(AddressDetails addressDetails) {
        if ( addressDetails == null ) {
            return null;
        }

        AddressEntity addressEntity = new AddressEntity();

        addressEntity.setAddressLine1( addressDetails.getAddressLine1() );
        addressEntity.setAddressLine2( addressDetails.getAddressLine2() );
        addressEntity.setCity( addressDetails.getCity() );
        addressEntity.setState( addressDetails.getState() );
        addressEntity.setZipCode( addressDetails.getZipCode() );
        addressEntity.setStatus( addressDetails.getStatus() );
        addressEntity.setError( addressDetails.getError() );

        return addressEntity;
    }
}
